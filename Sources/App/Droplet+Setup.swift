@_exported import Vapor

extension Droplet {
    public func setup() throws {
        let routes = try Routes(view, self)
        try collection(routes)
    }
}
