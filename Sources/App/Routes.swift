import Vapor
import PostgreSQL
import Sessions
import AuthProvider
import HTTP
import Crypto

final class Routes: RouteCollection {
    let view: ViewRenderer
    let postgreSQL: PostgreSQL.Database
    let authRoutes: RouteBuilder
    let loginRouteBuilder: RouteBuilder
    var hasLogged = false
    
    init(_ view: ViewRenderer,_ drop: Droplet) throws {
        self.view = view
        do {
            self.postgreSQL = try PostgreSQL.Database(hostname: "127.0.0.1", database: "webSema", user: "denisborodavcenko", password: "")
        } catch {
            throw Abort.serverError
        }
        //create a memory storage for our sessions
        let memory = MemorySessions()
        let sessionsMiddleware = SessionsMiddleware(memory)
        //create a password middleware with our user
        let passwordMiddleware = PasswordAuthenticationMiddleware(Admin.self)
        //create the persis middleware with our User
        let persistMiddleware = PersistMiddleware(Admin.self)
        self.authRoutes = drop.grouped([sessionsMiddleware, persistMiddleware, passwordMiddleware])
        self.loginRouteBuilder = drop.grouped([sessionsMiddleware, persistMiddleware])

    }
    
    func build(_ builder: RouteBuilder) throws {
        
        //MARK: STATIC PAGES GET REQUESTS
        builder.get("/") { req in
            return try PageController.init(self.view).index(req, hasLogged: self.hasLogged)
        }
        
        builder.get("about") { req in
            return try PageController.init(self.view).about(req, hasLogged: self.hasLogged)
        }
        
        //MARK: PANEL REQUEST GET/ POST/
        authRoutes.get("panel") { req in
            return try PanelController.init(self.view).panel(req, hasLogged: self.hasLogged)
        }
        
        authRoutes.post("panel") { req in
           return try PanelController.init(self.view).new_student(req)
        }
        
        
        authRoutes.post("update_student") { (req) -> ResponseRepresentable in
            return try PanelController.init(self.view).update_student(req)
        }
            
        authRoutes.delete("delete_student") { (req) in
            return try PanelController.init(self.view).delete_student(req)
        }
        
        //MARK: DATA REQUESTS GET/
        
        builder.get("/v1/get_schools") { req in
            guard let schools = API.shared.get_schools() else {
                throw Abort.serverError
            }
            return schools
        }
        
        builder.get("/v1/get_profiles") { req in
            guard let profiles = API.shared.get_profiles() else {
                throw Abort.serverError
            }
            return profiles
        }
        
        builder.get("/v1/get_exams") { req in
            guard let exams = API.shared.get_exams() else {
                throw Abort.serverError
            }
            return exams
        }
        
        //MARK: MAP REQUESTS GET/ POST/
        authRoutes.get("map") { req in
            return try MapController.init(self.view).map(req,hasLogged: self.hasLogged)
        }
        
        authRoutes.post("/v1/map") { req in
            return try MapController.init(self.view).new_school(req)
        }
        
        //MARK: REGISTRATION
        builder.get("login") { req in
            return try PageController.init(self.view).login(req, hasLogged: self.hasLogged)
        }
        
        builder.post("register") { req in
           return try AuthorizationController.init(self.view).register(req)
        }
        
        loginRouteBuilder.post("login") { req in
            return try AuthorizationController.init(self.view).login(req, completion: {
                self.hasLogged = true
            })
        }
        
        authRoutes.get("/logout") { (req) -> ResponseRepresentable in
            return try AuthorizationController.init(self.view).logout(req, completion: {
                self.hasLogged = false
            })
        }
        
        authRoutes.get("report") { (req) -> ResponseRepresentable in
            return try PageController.init(self.view).report(req)
        }
        
        authRoutes.get("make_report") { (req) -> ResponseRepresentable in
            return try ReportController.init(self.view).makeReport(req)
        }
        
        authRoutes.get("bar_diagram") { (req) -> ResponseRepresentable in
            return try ReportController.init(self.view).barDiagram(req)
        }
    }
}
