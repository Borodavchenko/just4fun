//
//  API.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 05.02.2018.
//

import Foundation
import Vapor
import HTTP

class API {
    static let shared = API()
    
    private init() { }
    
    func get_exams() -> JSON? {
        do {
            let exams = try Exam.all().makeJSON()
            return exams
        } catch let ex {
            print(ex)
        }
        return nil
    }
    
    func get_schools() -> JSON? {
        do {
            let schools = try School.all().makeJSON()
            return schools
        } catch let ex {
            print(ex)
        }
        return nil
    }
    
    func get_profiles() -> JSON? {
        do {
            let profiles = try Profile.all().makeJSON()
            return profiles
        } catch let ex {
            print(ex)
        }
        return nil
    }
}

