//
//  Utils.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 05.02.2018.
//
import Foundation
import Vapor
import PostgreSQL

class Utils {
    static let shared = Utils()
    private init() { }
    func getAllDataFromDatabase(completion: @escaping(_ users: [User]?, _ schools: [School]?, _ exams: [Exam]?, _ profiles: [Profile]?, _ mapExams: [MapExams]?, _ error: NSError?) -> Void) {
        do {
            let schools = try School.all()
            let profiles = try Profile.all()
            let exams = try Exam.all()
            let users = try User.all()
            let mapExams = try MapExams.all()
            completion(users, schools, exams, profiles, mapExams, nil)
        } catch let ex {
            print(ex)
            completion(nil, nil, nil, nil, nil, ex as NSError)
        }
    }
}
