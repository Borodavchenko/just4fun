//
//  Exam.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 13.01.2018.
//

import PostgreSQLProvider

final class Exam: Model {
    static let entity = "exams"
    var storage = Storage()
    var id: Int
    var name: String
//    var mark: Int
    
    init(id: Int, name: String/*, mark: Int*/) {
        self.id = id
        self.name = name
//        self.mark = mark
    }
    
    init(row: Row) throws {
        self.id = try row.get("id")
        self.name = try row.get("name")
//        self.mark = try row.get("mark")
    }
}

extension Exam: RowRepresentable {
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("id", id)
        try row.set("name", name)
//        try row.set("mark", mark)
        return row
    }
}

extension Exam: Preparation {
    static func prepare(_ database: Database) throws {
    }
    
    static func revert(_ database: Database) throws {
    }
}

extension Exam: JSONConvertible {
    convenience init(json: JSON) throws {
        try self.init(
            id: json.get("id"),
            name: json.get("name")
//            mark: json.get("mark")
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("id", id)
        try json.set("name", name)
//        try json.set("mark", mark)
        return json
    }
}
