//
//  School.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 06.01.2018.
//

import PostgreSQLProvider

final class School: Model {
    static let entity = "schools"
    var storage = Storage()
    var id: Int
    var name: String
    var lat: String
    var lng: String
    var info: String

    init(id: Int, name: String, info: String, lat: String, lng: String) {
        self.id = id
        self.name = name
        self.info = info
        self.lat = lat
        self.lng = lng
    }
    
    init(row: Row) throws {
        self.id = try row.get("id")
        self.name = try row.get("name")
        self.info = try row.get("info")
        self.lat = try row.get("lat")
        self.lng = try row.get("lng")
    }
}

extension School: RowRepresentable {
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("id", id)
        try row.set("name", name)
        try row.set("info", info)
        try row.set("lat", lat)
        try row.set("lng", lng)
        return row
    }
}

extension School: Preparation {
    static func prepare(_ database: Database) throws {
    }
    
    static func revert(_ database: Database) throws {
    }
}
extension School: JSONConvertible {
    convenience init(json: JSON) throws {
        try self.init(
            id: json.get("id"),
            name: json.get("name"),
            info: json.get("info"),
            lat: json.get("lat"),
            lng: json.get("lng")
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("id", id)
        try json.set("name",name)
        try json.set("lat",lat)
        try json.set("lng", lng)
        try json.set("info", info)
        return json
    }
}
