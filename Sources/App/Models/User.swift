//
//  User.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 13.01.2018.
//

import PostgreSQLProvider
import Foundation

final class User: Model {
    static let entity = "user"
    var storage = Storage()
    var id: Int
    var fio: String
    var email: String
    var login: String
    var password: String
    var phone_number: String
    var verified: Bool
    var reg_date: Date
    var enroll: Bool
    var school_id: Int
    var exams_id: Int
    var profile_id: Int
    
    init(id: Int, fio: String, phone_number: String, login: String, password: String, email: String, verified: Bool, reg_date: Date, enroll: Bool, school_id: Int, exams_id: Int, profile_id: Int) {
        self.id = id
        self.fio = fio
        self.phone_number = phone_number
        self.login = login
        self.password = password
        self.email = email
        self.verified = verified
        self.reg_date = reg_date
        self.enroll = enroll
        self.school_id = school_id
        self.exams_id = exams_id
        self.profile_id = profile_id
    }
    
    init(row: Row) throws {
        self.id = try row.get("id")
        self.fio = try row.get("fio")
        self.phone_number = try row.get("phone_number")
        self.login = try row.get("login")
        self.password = try row.get("password")
        self.email = try row.get("email")
        self.verified = try row.get("verified")
        self.reg_date = try row.get("reg_date")
        self.enroll = try row.get("enroll")
        self.school_id = try row.get("school_id")
        self.exams_id = try row.get("exams_id")
        self.profile_id = try row.get("profile_id")
    }
}

extension User: RowRepresentable {
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("id", id)
        try row.set("fio", fio)
        try row.set("phone_number", phone_number)
        try row.set("login", login)
        try row.set("password", password)
        try row.set("email", email)
        try row.set("verified", verified)
        try row.set("reg_date", reg_date)
        try row.set("enroll", enroll)
        try row.set("school_id", school_id)
        try row.set("exams_id", exams_id)
        try row.set("profile_id", profile_id)
        return row
    }
}

extension User: Preparation {
    static func prepare(_ database: Database) throws {
    }
    
    static func revert(_ database: Database) throws {
    }
}

extension User: JSONConvertible {
    convenience init(json: JSON) throws {
        try self.init(
            id: json.get("id"),
            fio: json.get("fio"),
            phone_number: json.get("phone_number"),
            login: json.get("login"),
            password: json.get("password"),
            email: json.get("email"),
            verified: json.get("verified"),
            reg_date: json.get("reg_date"),
            enroll: json.get("enroll"),
            school_id: json.get("school_id"),
            exams_id: json.get("exams_id"),
            profile_id: json.get("profile_id")
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("id", id)
        try json.set("fio",fio)
        try json.set("login",login)
        try json.set("password", password)
        try json.set("phone_number", phone_number)
        try json.set("email", email)
        try json.set("verified", verified)
        try json.set("reg_date", reg_date)
        try json.set("enroll", enroll)
        try json.set("school_id", school_id)
        try json.set("profile_id", profile_id)
        try json.set("exams_id", exams_id)
        return json
    }
}

