//
//  Profile.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 13.01.2018.
//

import PostgreSQLProvider

final class Profile: Model {
    static let entity = "profile"
    var storage = Storage()
    var id: Int
    var name: String
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    init(row: Row) throws {
        self.id = try row.get("id")
        self.name = try row.get("name")
    }
}

extension Profile: RowRepresentable {
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("id", id)
        try row.set("name", name)
        return row
    }
}

extension Profile: Preparation {
    static func prepare(_ database: Database) throws {
    }
    
    static func revert(_ database: Database) throws {
    }
}
extension Profile: JSONConvertible {
    convenience init(json: JSON) throws {
        try self.init(
            id: json.get("id"),
            name: json.get("name")
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("id", id)
        try json.set("name",name)
        return json
    }
}

