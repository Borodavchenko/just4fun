//
//  Admin.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 13.01.2018.
//

import PostgreSQLProvider
import AuthProvider

final class Admin: Model, PasswordAuthenticatable, SessionPersistable {
    static let entity = "admin"
    var storage = Storage()
    var id: Node?
    var email: String
    var password: String
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
    
    init(row: Row) throws {
        self.email = try row.get("email")
        self.password = try row.get("password")
    }
}

extension Admin: RowRepresentable {
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("email", email)
        try row.set("password", password)
        return row
    }
}

extension Admin: Preparation {
    static func prepare(_ database: Database) throws {
    }
    
    static func revert(_ database: Database) throws {
    }
}
extension Admin: JSONConvertible {
    convenience init(json: JSON) throws {
        try self.init(
            email: json.get("email"),
            password: json.get("password")
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("id", id)
        try json.set("email",email)
        try json.set("password", password)
        return json
    }
}
