//
//  MapExams.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 01.02.2018.
//

import Foundation
import PostgreSQLProvider

final class MapExams: Model {
    static let entity = "map_exams"
    var storage = Storage()
    var id: Int
    var user_id: Int
    var exam_id: Int
    
    init(id: Int, user_id: Int, exam_id: Int) {
        self.id = id
        self.user_id = user_id
        self.exam_id = exam_id
    }
    
    init(row: Row) throws {
        self.id = try row.get("id")
        self.user_id = try row.get("user_id")
        self.exam_id = try row.get("exam_id")
    }
}

extension MapExams: RowRepresentable {
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("id", id)
        try row.set("user_id", user_id)
        try row.set("exam_id", exam_id)
        return row
    }
}

extension MapExams: Preparation {
    static func prepare(_ database: Database) throws {
    }
    
    static func revert(_ database: Database) throws {
    }
}

extension MapExams: JSONConvertible {
    convenience init(json: JSON) throws {
        try self.init(
            id: json.get("id"),
            user_id: json.get("user_id"),
            exam_id: json.get("exam_id")
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("id", id)
        try json.set("user_id", user_id)
        try json.set("exam_id", exam_id)
        return json
    }
}
