//
//  ReportController.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 26.02.2018.
//

import Foundation
import HTTP
import Vapor
import PostgreSQL

final class ReportController {
    let postgreSQL: PostgreSQL.Database
    
    let view: ViewRenderer
    
    init(_ view: ViewRenderer) throws {
        self.view = view
        do {
            self.postgreSQL = try PostgreSQL.Database(hostname: "127.0.0.1", database: "webSema", user: "denisborodavcenko", password: "")
        } catch {
            throw Abort.serverError
        }
    }
    
    func makeReport(_ req: Request) -> ResponseRepresentable {
        let users = try! User.all()
        let reportJSON = try! users.makeJSON()
        return reportJSON
    }
    
    func barDiagram(_ req: Request) -> ResponseRepresentable {
        let users = try! User.all()
        let schools = try! School.all()
        var existSchools = [School]()
        for user in users {
            let school = schools.first(where: { $0.id == user.school_id })
            existSchools.append(school!)
        }
        let json = try! existSchools.makeJSON()
        return json
    }
}

