//
//  PanelController.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 05.02.2018.
//

import Foundation
import HTTP
import Vapor
import PostgreSQL

final class PanelController {
    var schoolsJSON: JSON?
    var examsJSON: JSON?
    var profileJSON: JSON?
    var usersJSON: JSON?
    var mapExamsJSON: JSON?
    let postgreSQL: PostgreSQL.Database

    let view: ViewRenderer
    init(_ view: ViewRenderer) throws {
        self.view = view
        do {
            self.postgreSQL = try PostgreSQL.Database(hostname: "127.0.0.1", database: "webSema", user: "denisborodavcenko", password: "")
        } catch {
            throw Abort.serverError
        }
    }
    
    func panel(_ req: Request, hasLogged: Bool) throws -> ResponseRepresentable {
        Utils.shared.getAllDataFromDatabase { (users, schools, exams, profiles, mapExams, error) in
            guard error == nil, let users = users, let schools = schools, let exams = exams, let profiles = profiles, let mapExams = mapExams else {
                print(error)
                return
            }
            do {
                self.schoolsJSON = try schools.makeJSON()
                self.examsJSON = try exams.makeJSON()
                self.profileJSON = try profiles.makeJSON()
                self.usersJSON = try users.makeJSON()
                self.mapExamsJSON = try mapExams.makeJSON()
            } catch let ex {
                print(ex)
            }
        }
        return try view.make("panel", ["schools": schoolsJSON!, "exams" : examsJSON!, "profiles":profileJSON!, "users": usersJSON!, "mapExams": mapExamsJSON!,"logged": hasLogged])
    }
    
    func new_student(_ req: Request) throws ->  ResponseRepresentable {
        guard let dict = req.formURLEncoded else {
            throw Abort.badRequest
        }
        guard let firstName = dict["first_name"]?.string, let lastName = dict["last_name"]?.string, let phoneNumber = dict["phone_number"]?.string, let school = dict["school"]?.string, let profile = dict["profile"]?.string, let exams = dict["exams"]?.array, let enroll = dict["enroll"]?.bool, let verified = dict["verified"]?.bool else {
            throw Abort.badRequest
        }
        var count = try (User.all().sorted(by: { $0.id > $1.id }).first?.id)?.int
        count = count! + 1
        let schoolObj = try School.all().first(where: {$0.name == school})?.id
        let profileObj = try Profile.all().first(where: {$0.name == profile})?.id
        let database = try self.postgreSQL.makeConnection()
        do {
            var examsId = [Int]()
            for exam in exams.array {
                let exams = try Exam.all()
                guard let id = exams.first(where: {$0.name == exam.string})?.id else {
                    throw Abort.badRequest
                }
                examsId.append(id.int!)
            }
            try database.execute("INSERT INTO \"user\" VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)",[count,firstName + " " + lastName, "", "", Date(), enroll, verified, "", phoneNumber, schoolObj?.int, profileObj?.int, count])
            let lastUserId = try (User.all().last?.id)!.int! + 1
            for examId in examsId {
                try database.execute("INSERT INTO map_exams VALUES ($1,$2)",[count,examId])
            }
        } catch let ex {
            print(ex)
        }
        let status = Status(statusCode: 202, reasonPhrase: "success")
        return try Response(status: status , json: nil)
    }
    
    func update_student(_ req: Request) throws -> ResponseRepresentable {
        guard let dict = req.formURLEncoded else {
            throw Abort.badRequest
        }
        guard let firstName = dict["first_name"]?.string, let lastName = dict["last_name"]?.string, let phoneNumber = dict["phone_number"]?.string, let school = dict["school"]?.string, let profile = dict["profile"]?.string,  let enroll = dict["enroll"]?.bool, let verified = dict["verified"]?.bool, let id = dict["id"]?.string else {
            throw Abort.badRequest
        }
        let schoolObj = try School.all().first(where: {$0.name == school})?.id
        let profileObj = try Profile.all().first(where: {$0.name == profile})?.id
        let database = try self.postgreSQL.makeConnection()
        do {
            guard let school_id = schoolObj?.int, let profile_id = profileObj?.int else {
                throw Abort.badRequest
            }
            try database.execute("UPDATE \"user\" SET fio='\(firstName + " " + lastName)',enroll=\(enroll),verified=\(verified),phone_number='\(phoneNumber)',school_id=\(school_id),profile_id=\(profile_id) WHERE id=\(id)")
        } catch let ex {
            print(ex)
        }
        let status = Status(statusCode: 202, reasonPhrase: "success")
        return try Response(status: status , json: nil)
    }
    
    func delete_student(_ req: Request) throws -> ResponseRepresentable {
        guard let dict = req.formURLEncoded else { throw Abort.init(Status(statusCode: 402, reasonPhrase: "Empty data")) }
        guard let user_ids = dict["user_ids"]?.array else { throw Abort.serverError }
        var users = [User]()
        for id in user_ids.array {
            guard let user = try User.all().first(where: { $0.id == id.int }) else { throw Abort.serverError }
            users.append(user)
        }
        var mapExams = [MapExams]()
        for user in users {
            let mapExs = try MapExams.all().filter({$0.user_id == user.id})
            mapExams.append(contentsOf: mapExs)
        }
        for mapExam in mapExams {
            try mapExam.delete()
        }
        for user in users {
            try user.delete()
        }
        let status = Status(statusCode: 202, reasonPhrase: "success")
        return try Response(status: status , json: nil)
    }
    
}
