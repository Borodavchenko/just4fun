//
//  MapController.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 05.02.2018.
//

import Foundation
import HTTP
import PostgreSQL

final class MapController {
    let postgreSQL: PostgreSQL.Database

    let view: ViewRenderer
    init(_ view: ViewRenderer) throws {
        self.view = view
        do {
            self.postgreSQL = try PostgreSQL.Database(hostname: "127.0.0.1", database: "webSema", user: "denisborodavcenko", password: "")
        } catch {
            throw Abort.serverError
        }
    }
    
    func map(_ req: Request, hasLogged: Bool) throws -> ResponseRepresentable {
        return try view.make("map", ["logged": hasLogged])
    }
    
    func new_school(_ req: Request) throws -> ResponseRepresentable {
        guard let dict = req.formURLEncoded else {
            throw Abort.badRequest
        }
        guard let lat = dict["lat"]?.string, let lng = dict["lng"]?.string, let name = dict["name"]?.string else {
            throw Abort.badRequest
        }
        let database = try self.postgreSQL.makeConnection()
        let count = try School.all().last?.id
        do {
            try database.execute("INSERT INTO schools VALUES ($1,$2,$3,$4,$5)",[(count?.int)!+1,name, "hui", lat, lng])
        } catch let ex {
            print(ex)
        }
        let status = Status(statusCode: 202, reasonPhrase: "success")
        return try Response(status: status , json: nil)
    }
}
