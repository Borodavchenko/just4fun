//
//  AuthorizationController.swift
//  webSemaPackageDescription
//
//  Created by Денис Бородавченко on 05.02.2018.
//

import Foundation
import HTTP
import AuthProvider


final class AuthorizationController {
    
    let view: ViewRenderer
    init(_ view: ViewRenderer) {
        self.view = view
    }
    
    func register(_ req: Request) throws -> ResponseRepresentable {
        guard let dict = req.formURLEncoded else {
            throw Abort.serverError
        }
        if let email = dict["email"]?.string,
            !email.isEmpty,
            let password = dict["password"]?.string,
            !password.isEmpty {
            let user = Admin(email: email, password: password)
            try user.save()
        }
        let status = Status(statusCode: 202, reasonPhrase: "success")
        return try Response(status: status , json: nil)
    }
    func login(_ req: Request, completion: @escaping() -> Void ) throws -> ResponseRepresentable {
        guard let email = req.formURLEncoded?["email"]?.string, let password = req.formURLEncoded?["password"]?.string  else {
            throw Abort.badRequest
        }
        //create a Password object with email and password
        let credentials = Password(username: email, password: password)
        //User.authenticate queries the user by username and password and informs the middlewar that this user is now authenticated
        //the middleware creates a session token, ties it to the user and sends it in a cookie to the client.
        //the requests done with this request token automatically are authenticated with this user.
        let user = try Admin.authenticate(credentials)
        req.auth.authenticate(user)
        //redirect to the protected route /hello
        completion()
        return Response(redirect: "/")
    }
    func logout(_ req: Request, completion: @escaping() -> Void) throws -> ResponseRepresentable {
        try req.auth.unauthenticate()
        completion()
        return Response(redirect: "/")
    }
}
