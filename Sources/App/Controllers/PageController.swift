import Vapor
import HTTP

final class PageController {
    
    let view: ViewRenderer
    init(_ view: ViewRenderer) {
        self.view = view
    }

    func index(_ req: Request, hasLogged: Bool) throws -> ResponseRepresentable {
        return try view.make("base", ["logged": hasLogged])
    }
    
    func login(_ req: Request, hasLogged: Bool) throws -> ResponseRepresentable {
        return try view.make("login", ["logged": hasLogged])
    }
    
    func about(_ req: Request, hasLogged: Bool ) throws -> ResponseRepresentable {
        return try view.make("about",  ["logged": hasLogged])
    }
    
    func report(_ req: Request) throws -> ResponseRepresentable {
        return try view.make("report")
    }

}
