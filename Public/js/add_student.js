$('#add_student').click(function() {
                        $('#add_modal').plainModal('open', {duration: 500});
                        });
$('#add_stud').click(function(){
                     if($('#first_name').val() === "") {
                        alert("Введите имя");
                     } else if($('#last_name').val() === "") {
                        alert("Введите фамилию");
                     } else if($('#email_field').val() === "") {
                        alert("Введите email");
                     } else if($('#phone_number').val() === ""){
                        alert("Введите номер телефона");
                     }
                     var myData = {};
                     myData["first_name"] = $('#first_name').val();
                     myData["last_name"] =$('#last_name').val();
                     myData["email"] = $('#email_field').val();
                     myData["phone_number"] = $('#phone_number').val();
                     myData["school"] = $('#school_select').val();
                     myData["profile"] = $('#profile_select').val();
                     myData["exams"] = $('#exams_select').val();
                     myData["enroll"] = $('#enroll').is(':checked');
                     myData["verified"] = $('#verified').is(':checked');
                     jQuery.ajax({
                                 type: "POST",
                                 url: "/panel",
                                 dataType: "json",
                                 data: myData,
                                 success: function(e) {
//                                 alert(e);
                                 location.reload();
                                 },
                                 error:function (xhr, ajaxOptions, thrownError){
                                 alert(xhr);
                                 alert(thrownError); //выводим ошибку
                                 }
                                 });
                     });

$('#delete_student').click(function () {
                           var items = getValueUsingClass();
                           var data = {};
                           data["user_ids"] = items;
                           jQuery.ajax({
                                       type: "DELETE",
                                       url: "/delete_student",
                                       dataType: "json",
                                       data: data,
                                       success: function(e) {
//                                       alert(e);
                                       location.reload();
                                       },
                                       error:function (xhr, ajaxOptions, thrownError){
                                       alert(xhr);
                                       alert(thrownError); //выводим ошибку
                                       }
                                       });
                           });
var id = 0;
$('tr').click(function () {
              var items = getValueUsingClass();
              var data = {};
              var tr = $(this);
              var dict = {};
              for(var i = 0; i < tr[0].children.length; i++) {
              dict[i] = tr[0].children[i].innerText;
              }
              id = dict[0];
              var last_name = dict[1].split(" ")[1];
              var first_name = dict[1].split(" ")[0];
              $('#edit_first_name').val(first_name);
              $('#edit_last_name').val(last_name);
              $('#edit_phone_number').val(dict[2]);
              $('#edit_school_select').val(dict[3]);
              $('#edit_profile_select').val(dict[4]);
              var exams = dict[5].split(",");
              exams.forEach(function(e) {
                            var ex = e;
                            $("#edit_exams_select > option").each(function(s, name) {
                                                                  if (name.value == ex) {
                                                                  name.selected = true;
                                                                  }
                                                                  });
                           });
              $('#edit_enroll')[0].checked = dict[6];
              $('#edit_verified')[0].checked = dict[7];
              if(items.length == 0) {
              $('#edit_modal').plainModal('open', {duration: 1000});
              }
                                    });
              
$('#save_stud').click(function() {
                      var myData = {};
                      myData["id"] = id;
                      myData["first_name"] = $('#edit_first_name').val();
                      myData["last_name"] =$('#edit_last_name').val();
                      myData["email"] = $('#edit_email_field').val();
                      myData["phone_number"] = $('#edit_phone_number').val();
                      myData["school"] = $('#edit_school_select').val();
                      myData["profile"] = $('#edit_profile_select').val();
                      myData["exams"] = $('#edit_exams_select').val();
                      myData["enroll"] = $('#edit_enroll').is(':checked');
                      myData["verified"] = $('#edit_verified').is(':checked');
                      jQuery.ajax({
                                  type: "POST",
                                  url: "/update_student",
                                  dataType: "json",
                                  data: myData,
                                  success: function(e) {
//                                  alert(e);
                                  location.reload();
                                  },
                                  error:function (xhr, ajaxOptions, thrownError){
                                  alert(xhr);
                                  alert(thrownError); //выводим ошибку
                                  }
                                  });
                      });

function getValueUsingClass(){
    var chkArray = [];
    
    $(".chk_for_actions:checked").each(function() {
                           chkArray.push($(this).val());
                           });
    
    var selected;
    selected = chkArray.join(',') ;
    if(selected.length > 0){
        console.log("You have selected " + selected);
    }else{
        console.log("Please at least one of the checkbox");
    }
    return chkArray;
}
