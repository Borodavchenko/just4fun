jQuery.ajax({
            type: "GET",
            url: "/make_report",
            dataType: "json",
            success: function(e) {
            var verified = 0;
            var unverified = 0;
            for (var i=0;i<e.length;i++) {
            if(e[i]["verified"]) {
            verified++;
            } else {
            unverified++;
            }
            }
            var data = [{
                        values: [unverified, verified],
                        labels: ['Неподтвержденные', 'Подтвержденные'],
                        type: 'pie'
                        }];
            var layout = {
            title: 'Сводка пользователей',
            height: 400,
            width: 800
            };
            Plotly.newPlot('report', data, layout);
            },
            error:function (xhr, ajaxOptions, thrownError){
            alert(xhr);
            alert(thrownError); //выводим ошибку
            }
            });
jQuery.ajax({
            type: "GET",
            url: "/bar_diagram",
            dataType: "json",
            success: function(e) {
            var schools = [];
            var counts = []
            for (var i=0;i<e.length;i++) {
            schools[i] = e[i]["name"];
            counts[i] = 1;
            }
            schools.forEach(function(e) {
                            if(e == "МБОУ СОШ №20") {
                            counts[schools.indexOf(e)] = 2;
                            }
                            });
            var data = [
                        {
                        x: schools,
                        y: counts,
                        name: 'Школы',
                        type: 'bar'
                        }
                        ];
            
            Plotly.newPlot('bar_report', data);
            },
            error:function (xhr, ajaxOptions, thrownError){
            alert(xhr);
            alert(thrownError); //выводим ошибку
            }
            });
