/**
 * Created by denisborodavcenko on 09.07.16.
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//$.ajax({
//    url:'output.php',//запрос данных из базы
//    type:'POST',
//    data:  {},
//    success: function(res) {
//        res1=[];
//        res1 = JSON.parse(res);//парсинг в массив
//    }
//
//});
var infowindow;
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
                                             var latitude = position.coords.latitude;
                                             var longitude = position.coords.longitude;
                                             var coords = new google.maps.LatLng(latitude, longitude);
                                             var mapOptions = {
                                             zoom: 13,
                                             center: coords,
                                             mapTypeControl: true,
                                             navigationControlOptions: {
                                             style: google.maps.NavigationControlStyle.SMALL
                                             },
                                             mapTypeId: google.maps.MapTypeId.ROADMAP
                                             };
                                             
                                             var map = new google.maps.Map(
                                                                           document.getElementById("map"), mapOptions
                                                                           );
                                             var markers = [];
                                             infowindow = new google.maps.InfoWindow({map:map});
                                             jQuery.ajax({
                                                         type: "GET",
                                                         url: "/v1/get_schools",
                                                         dataType: "json",
                                                         success: function(e) {
                                                         for (var i=0;i<e.length;i++) {
                                                         placeMarker(e[i],map);
                                                         }
                                                         }
                                                         });
                                            
                                             map.addListener('click', function(e){
//                                                             alert(e);
                                                             placeMarker(e.latLng,map);
                                                             var myData = {};
                                                             myData["lat"] = e.latLng.lat;
                                                             myData["lng"] = e.latLng.lng;
                                                             myData["name"] = prompt("Введите название");
                                                             jQuery.ajax({
                                                                         type: "POST",
                                                                         url: "/v1/map",
                                                                         dataType: "json",
                                                                         data: myData,
                                                                         success: function(e) {
                                                                         jQuery.ajax({
                                                                                     type: "GET",
                                                                                     url: "/v1/get_schools",
                                                                                     dataType: "json",
                                                                                     success: function(e) {
                                                                                     for (var i=0;i<e.length;i++) {
                                                                                     placeMarker(e[i],map);
                                                                                     }
                                                                                     }
                                                                                     });
                                                                         }
                                                                         });
                                                             });
                                             });
                                             //ФИЛЬТРЫ ДЛЯ МАРКЕРОВ
                                             /*ШКОЛЫ*/
                                             //        $(document).ready(function() {
                                             //ЕСЛИ НАЖАТ ЧЕКБОКС "ШКОЛЫ" ВЫВОДИМ МАРКЕРЫ
                                             //           $("#but1").click(function () {
                                             //
                                             //                if (but1.checked) {
                                             //                    for (var i = 0; i <= res1.length; i++) {
                                             //                        if(res1[i].type=="Ш"){
                                             //                        var info = '<p>' + res1[i].name + '</p>' + '<p>' + res1[i].adr + '</p>' + "<p>" + res1[i].contact + '</p>';
                                             //                        contentStr = '<p>' + info + '</p>';
                                             //                        markers[i] = new google.maps.Marker({
                                             //                            position: new google.maps.LatLng(res1[i].lat, res1[i].lng),
                                             //                            title: res1[i].name,
                                             //                            map: map,
                                             //                            buborek: contentStr
                                             //                        });
                                             //                        google.maps.event.addListener(markers[i], 'click', function () { //ВЫВОД ОКНА С ИНФОРМАЦИЕЙ
                                             //                            infowindow.setContent(this.buborek);
                                             //                            infowindow.open(map, this);
                                             //                        });
                                             //                        markers[i].setMap(map);
                                             //                    }
                                             //                }}
                                             //            //ЕСЛИ СНЯТА ГАЛОЧКА С ЧЕКБОКСА, УБИРАЕМ МАРКЕРЫ
                                             //                else if(but1.checked==false){
                                             //                    for (var i = 0; i <= res1.length; i++) {
                                             //                        if(res1[i].type=="Ш"){
                                             //                        markers[i].setMap(null);
                                             //                    }}
                                             //                }
                                             //            })
                                             //
                                             //            /*ТЕХНИКУМЫ*/
                                             //            $("#but2").click(function () {
                                             //
                                             ////ЕСЛИ НАЖАТ ЧЕКБОКС "ТЕХНИКУМЫ" ВЫВОДИМ МАРКЕРЫ
                                             //                if (but2.checked) {
                                             //                    for (var i = 0; i <= res1.length; i++) {
                                             //                        if(res1[i].type=="С"){
                                             //                        var info = '<p>' + res1[i].name + '</p>' + '<p>' + res1[i].adr + '</p>' + "<p>" + res1[i].contact + '</p>';
                                             //                        contentStr = '<p>' + info + '</p>';
                                             //                        markers[i] = new google.maps.Marker({
                                             //                            position: new google.maps.LatLng(res1[i].lat, res1[i].lng),
                                             //                            title: res1[i].name,
                                             //                            map: map,
                                             //                            buborek: contentStr
                                             //                        });
                                             //                        google.maps.event.addListener(markers[i], 'click', function () {
                                             //                            infowindow.setContent(this.buborek);
                                             //                            infowindow.open(map, this);
                                             //                        });
                                             //                        markers[i].setMap(map);
                                             //                    }
                                             //                }}
                                             //                //ЕСДИ НЕТ, УБИРАЕМ МАРКЕРЫ
                                             //                else if(but2.checked==false){
                                             //                    for (var i = 4; i <= res1.length; i++) {
                                             //                        if(res1[i].type=="С"){
                                             //                            markers[i].setMap(null);
                                             //                    }}
                                             //                }
                                             //            })
                                             //        })
} else {
    alert("NE PODDERZHIVAETSA");
}


function placeMarker(location, map) {
    var coords = new google.maps.LatLng(location.lat, location.lng);
    var marker = new google.maps.Marker({
                                        position: coords,
                                        map: map
                                        });
    marker.setMap(map);
    google.maps.event.addListener(marker, 'click', function() {
                                  infowindow.setContent(location.name);
                                  infowindow.open(map, this);
                                  });

}
function callback(results, status, map) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            var place = results[i];
            createMarker(results[i], map);
        }
    }
}
function createMarker(place,map) {
    var placeLoc = place.geometry.location;
    var marker = new google.maps.Marker({
                                        map: map,
                                        position: place.geometry.location
                                        });
    marker.setMap(map)
    google.maps.event.addListener(marker, 'click', function() {
                                  infowindow.setContent(place.name);
                                  infowindow.open(map, this);
                                  });
}




